#include<stdio.h>

#define LENGTH  8

void print_list(int list[], int len, char *comment)
{
    printf("%s", comment);
    printf("[ ");
    for (int i = 0; i < len; i++) { printf("%d ", list[i]); }
    printf("]\n");
}

void insert_sort(int array[], int length){
    for (int j=1; j< length; j++) {
        int key = array[j];
        int i = j - 1;
        while (i>0 && array[i] > key) {
            array[i+1] = array[i];
            i = i - 1;
        }
        array[i+1] = key;
    }
}

int main(){
    int array[LENGTH] = {1, 5, 4, 10, 2, 8, 16, 9};
    print_list(array, LENGTH, "Before sort: ");
    insert_sort(array, LENGTH);
    print_list(array, LENGTH, "After sort: ");
    return 0;
}
