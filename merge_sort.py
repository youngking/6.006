#!/usr/bin/env python
# encoding: utf-8


def merge_sort(array):
    if len(array) <= 1:
        return array
    mid = int(len(array) / 2)
    left = merge_sort(array[0:mid])
    right = merge_sort(array[mid:])
    return merge(left, right)


def merge(left, right):
    merged = []
    while left and right:
        merged.append(left.pop(0) if left[0] <= right[0] else right.pop(0))
    if left:
        merged.extend(left)
    elif right:
        merged.extend(right)
    return merged


def main():
    array = [1, 5, 4, 10, 2, 8, 16, 9]
    print("排序前: %s" % array)
    array = merge_sort(array)
    print("排序后: %s" % array)

if __name__ == '__main__':
    main()
