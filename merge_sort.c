#include<stdio.h>

#define LENGTH  9

void print_list(int list[], int len, char *comment)
{
    printf("%s", comment);
    printf("[ ");
    for (int i = 0; i < len; i++) { printf("%d ", list[i]); }
    printf("]\n");
}

void merge(int array[], unsigned int left, unsigned mid, unsigned right){
    int left_length = mid - left;
    int right_length = right - mid;

    //初始化临时数组
    int left_half[left_length];
    int right_half[right_length];
    for (int i=left, l=0; i<mid; i++, l++){
        left_half[l] = array[i];
    }
    for(int i=mid, r=0; i<right; i++, r++){
        right_half[r] = array[i];
    }

    // 合并临时数组
    int index = left ; // 数组游标位置
    int l=0, r = 0; // 左右两部分临时数组的游标
    for (; l < left_length && r < right_length; index++ ){
        if (left_half[l] < right_half[r]) {
            array[index] = left_half[l];
            l++;
        } else {
            array[index] = right_half[r];
            r++;
        }
    }


    for(; l< left_length; index++, l++){
        array[index] = left_half[l];
    }

    for(; r< right_length; index++, r++){
        array[index] = right_half[r];
    }

}



//合并排序
void merge_sort(int array[], unsigned int left, unsigned right){
    if (right - left <=1 ) {return; }
    int mid = left + (right - left)/2;
    // 对数组左边排序
    merge_sort(array, left, mid);
    // 对数组右边排序
    merge_sort(array, mid, right);
    // 左右边合并
    merge(array, left, mid, right);
}



int main(){
    int array[LENGTH] = {1, 5, 4, 10, 2, 8, 16, 9, 11};
    print_list(array, LENGTH, "Before sort: ");
    merge_sort(array, 0, LENGTH);
    print_list(array, LENGTH, "After sort: ");
    return 0;
}
