#!/usr/bin/env python
# encoding: utf-8


def insert_sort(array):
    for j in range(1, len(array)):
        key = array[j]
        i = j - 1
        while i > 0 and array[i] > key:
            array[i + 1] = array[i]
            i = i - 1
        array[i + 1] = key


def main():
    array = [1, 5, 4, 10, 2, 8, 16, 9]
    print("排序前: %s" % array)
    insert_sort(array)
    print("排序后: %s" % array)

if __name__ == '__main__':
    main()
